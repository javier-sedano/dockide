#!/bin/bash -ex
pushd `dirname $0`/..

TAG="$1"
if [ -z "$TAG" ]
then
    TAG="0.0.0-0"
fi

dotnet restore
dotnet tool restore

dotnet publish -c Release -o published/linux --runtime linux-x64 --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true -p:DebugType=None -p:DebugSymbols=false /p:Version=$TAG
cd published/linux
install -d ../../delivery/$TAG/linux/
zip ../../delivery/$TAG/linux/Dockide.zip Dockide
cd ../..

dotnet publish -c Release -o published/linux_slim --runtime linux-x64 --self-contained false -p:PublishSingleFile=true -p:DebugType=None -p:DebugSymbols=false /p:Version=$TAG
cd published/linux_slim
install -d ../../delivery/$TAG/linux_slim/
zip ../../delivery/$TAG/linux_slim/Dockide.zip Dockide
cd ../..

popd
