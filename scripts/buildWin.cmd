cd /D %~dp0
cd ..

set TAG=%1
if "%TAG%" == "" (
    set TAG="0.0.0-0"
)

set SZ="7z"

dotnet restore
dotnet tool restore

dotnet publish -c Release -o published/win --runtime win-x64 --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true -p:DebugType=None -p:DebugSymbols=false /p:Version=%TAG%
cd published\win
mkdir ..\..\delivery\%TAG%\win\
%SZ% a ..\..\delivery\%TAG%\win\Dockide.zip Dockide.exe
cd ..\..

dotnet publish -c Release -o published/win_slim --runtime win-x64 --self-contained false -p:PublishSingleFile=true -p:DebugType=None -p:DebugSymbols=false /p:Version=%TAG%
cd published\win
mkdir ..\..\delivery\%TAG%\win_slim\
%SZ% a ..\..\delivery\%TAG%\win_slim\Dockide.zip Dockide.exe
cd ..\..
