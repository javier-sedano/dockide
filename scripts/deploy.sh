#!/bin/bash -ex

pushd `dirname $0`/..

DEPLOY="deploy"

AUTH_GITLAB=""
if [ "$GITLAB_WEB_TOKEN" ]
then
    AUTH_GITLAB="oauth2:$GITLAB_WEB_TOKEN@"
fi
WEB_GIT_URL="https://${AUTH_GITLAB}gitlab.com/javier-sedano/dockideweb.git"

VERSION="$1"
TAG="$2"
if [ -z "$VERSION" -o -z "$TAG" ]
then
  echo "Usage: $0 <version> <tag>"
  exit 1
fi

configure_git(){
    git config --global user.name 'Dockideweb Bot'
    git config --global user.email 'javier.sedano+dockideweb@gmail.com'
}

clone_web_git() {
    rm -rf $DEPLOY
    install -d $DEPLOY
    cd $DEPLOY
    git clone "$WEB_GIT_URL" .
    cd ..
}

publish_to_web() {
    cd $DEPLOY
    rm -rf downloads/${VERSION}*
    cd downloads
    cp -r ../../delivery/$TAG/ .
    cd ..
    cd ..
}

push_to_web_git() {
    cd $DEPLOY
    git add .
    COMMIT_MESSAGE="CD from $CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA on $(date)"
    git commit -m"$COMMIT_MESSAGE"
    git push
    cd ..
}

configure_git
clone_web_git
publish_to_web
push_to_web_git

popd
