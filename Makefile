ifeq ($(VERSION),)
	VERSION := 0.0.0-dev
endif
PVERSION=/p:Version=$(VERSION)

dependenciesInit:
	dotnet restore
	dotnet tool restore

clean:
	rm -rf dockide/bin/
	rm -rf dockide/obj/
	rm -rf published/
	rm -rf .sonarqube/

build:
	dotnet publish -c Release -o published --runtime win-x64 --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true -p:DebugType=None -p:DebugSymbols=false $(PVERSION) ;

rebuild: clean build

sonar:
	dotnet sonarscanner begin \
		/k:com.odroid.dockide /n:dockide \
		/v:0.0.0 /d:sonar.host.url=http://sonarqube:9000 
	dotnet build
	dotnet sonarscanner end
	@echo "If dockerized, browse http://localhost:9009"
