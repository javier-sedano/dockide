Run your complete development environment inside docker-compose

WARNING: EARLY ALPHA

Keywords: C#, .NET Core, console, xunit, sonarqube, vscode, docker, docker-compose, dockide

Contributions are welcome: just create a fork and create a Merge Request and we will evaluate it.

For user instructions, visit the web page at: https://javier-sedano.gitlab.io/dockideweb/

Development instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Clone from git.
  * WARNING: uses git-lfs, so clone with LFS enabled (default behaviour in GitForWindows; git-lfs package in Linux)
* Open folder in VSCode
  * Remote container will be detected, and offered to "Reopen in container". Do so.
* Makefile targets will be shown in Task Explorer
  * Run "dependenciesInit" to download dependencies (command line: make dependenciesInit)
    * You may need to restart the Omnisharp plugin for it to find the dependencies: use the VSCode command palete (Ctrl-Shift-P) and `OmniSharp: Restart OmniSharp`
  * Run "rebuildDev" to create only the Windows standalone binary, "build" for all targets
* PoC in Dockide/tests/poc/ (see README.md)

Useful links:
* https://docs.microsoft.com/en-us/dotnet/fundamentals/
* https://avaloniaui.net/
* https://sourceforge.net/p/vcxsrv/wiki/Home/
* https://www.x.org/archive/current/doc/man/man1/xauth.1.xhtml
* https://docs.gitlab.com/ee/user/gitlab_com/index.html#windows-shared-runners-beta

Development principles:

* SOLID, DRY, Clean code,...
* TDD, BDD
* Branch-based development

