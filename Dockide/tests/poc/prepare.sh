#!/bin/bash -e
DOCKIDE_FILES=dockide_files/

pushd "$(dirname "$0")"

rm -rf $DOCKIDE_FILES
mkdir $DOCKIDE_FILES
cd $DOCKIDE_FILES
# On real dockide, this will be downloaded
unzip ../vcxsrv-64.1.20.8.1.installer.zip

popd
