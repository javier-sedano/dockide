* Unzip vcxsrv-64.1.20.8.1.installer.zip to dockide_files/vcxsrv-64.1.20.8.1.installer or use prepara.sh (linux)

    It will be done in-program in the real dockide

* Run dockide_start.cmd in Windows

    It will be an .exe in real dockide

* When finished, run dockide_stop.cmd
