cd /D %~dp0
docker-compose -f .dockide\docker-compose.yml up --build -d

FOR /F "tokens=* USEBACKQ" %%g IN (`docker exec -ti dockide_poc_1 dig +short host.docker.internal`) do (SET "IP=%%g")

del dockide_files\.Xauthority
: In real dockide this key must be longer and hex (see https://www.x.org/archive/X11R6.8.1/doc/xauth.1.html)
set KEY=00000%random%
set KEY=%KEY:~-4%
dockide_files\vcxsrv-64.1.20.8.1.installer\xauth -f dockide_files\.Xauthority add %IP%%:0 . %KEY%
docker cp dockide_files\.Xauthority dockide_poc_1:/root/.Xauthority
start /B dockide_files\vcxsrv-64.1.20.8.1.installer\vcxsrv.exe :0 -multiwindow -auth ..\.Xauthority
start /B docker exec -e DISPLAY=host.docker.internal:0 dockide_poc_1 xcalc
